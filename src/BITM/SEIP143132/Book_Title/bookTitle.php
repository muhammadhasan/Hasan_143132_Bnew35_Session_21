<?php


namespace App\BookTitle;

require_once ("../../../../vendor/autoload.php");

use App\Model\Database as DB;

class BookTitle extends DB{
    public $id;
    public $book_title;
    public $author_name;

    public function __construct(){

        parent::__construct();
    }

}
$objBookTitle=new BookTitle();